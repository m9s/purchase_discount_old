#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Purchase Discount',
    'name_de_DE': 'Einkauf Rabatt',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Define discounts for purchase lines
    - Adds field discount in report purchase
''',
    'description_de_DE': '''
    - Ermöglicht die Eingabe von Rabatten pro Einkaufsposition
    - Fügt Rabattfeld im Bericht Einkauf hinzu
''',
    'depends': [
        'purchase',
        'account_invoice_discount'
        ],
    'xml': [
        'purchase.xml'
        ],
    'translation': [
        'locale/de_DE.po'
    ],
}
