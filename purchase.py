#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Equal, Eval
from trytond.transaction import Transaction
from trytond.pool import Pool


class PurchaseLine(ModelSQL, ModelView):
    _name = 'purchase.line'

    discount = fields.Numeric('Discount %', digits=(16, 2), states={
                'invisible': Not(Equal(Eval('type'), 'line'))
        }, depends=['type'])

    def __init__(self):
        super(PurchaseLine, self).__init__()
        self.amount = copy.copy(self.amount)
        if self.amount.on_change_with:
            self.amount.on_change_with.extend(['discount'])
        self._reset_columns()

    def default_discount(self):
        return 0.0

    def on_change_with_amount(self, vals):
        if vals.get('type') == 'line' and vals.get('discount'):
            vals = vals.copy()
            vals['unit_price'] = (vals.get('unit_price') -
                vals.get('unit_price') * vals.get('discount') * Decimal('0.01'))
        return super(PurchaseLine, self).on_change_with_amount(vals)

    def get_amount(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        res = super(PurchaseLine, self).get_amount(ids, name)
        for line in self.browse(ids):
            if line.type == 'line':
                currency = line.purchase and line.purchase.currency \
                        or line.currency
                res[line.id] = currency_obj.round(currency,
                        Decimal(str(line.quantity)) * line.unit_price -
                            (Decimal(str(line.quantity)) * line.unit_price *
                            (line.discount * Decimal('0.01'))))
        return res

    def get_invoice_line(self, line):
        res = super(PurchaseLine, self).get_invoice_line(line)[0]
        if line.type != 'line':
            return [res]
        if res['quantity'] <= 0.0:
            return None

        res['discount'] = line.discount
        return [res]

PurchaseLine()

class Purchase(ModelSQL, ModelView):
    _name = 'purchase.purchase'

    def on_change_lines(self, vals):
        if vals.get('lines'):
            vals = vals.copy()
            lines = []
            for line in vals['lines']:
                if line.get('discount'):
                    line['unit_price'] = (line.get('unit_price')-
                              line.get('unit_price') * line.get('discount') *
                              Decimal('0.01'))
                lines.append(line)
            vals['lines'] = lines
        return super(Purchase, self).on_change_lines(vals)

    def get_tax_amount(self, purchases):
        '''
        Return the tax amount for each purchases

        :param purchases: a BrowseRecordList of purchases
        :return: a dictionary with purchase id as key and
            the tax amount as value
        '''
        pool = Pool()
        currency_obj = pool.get('currency.currency')
        tax_obj = pool.get('account.tax')
        invoice_obj = pool.get('account.invoice')

        res = {}
        for purchase in purchases:
            context = self.get_tax_context(purchase)
            res.setdefault(purchase.id, Decimal('0.0'))
            taxes = {}
            for line in purchase.lines:
                if line.type != 'line':
                    continue
                price = line.unit_price - line.unit_price * line.discount / Decimal('100')
                # Don't round on each line to handle rounding error
                with Transaction().set_context(**context):
                    taxes_compute = tax_obj.compute(
                            [t.id for t in line.taxes], price,
                            line.quantity)

                for tax in taxes_compute:
                    key, val = invoice_obj._compute_tax(tax, 'in_invoice')
                    if not key in taxes:
                        taxes[key] = val['amount']
                    else:
                        taxes[key] += val['amount']
            for key in taxes:
                res[purchase.id] += currency_obj.round(purchase.currency,
                        taxes[key])
            res[purchase.id] = currency_obj.round(purchase.currency,
                        res[purchase.id])
        return res

Purchase()
